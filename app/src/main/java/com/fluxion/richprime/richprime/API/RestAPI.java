package com.fluxion.richprime.richprime.API;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by fluxion inc on 29/06/2017.
 */

public class RestAPI {

    public String httpGETMethod(String serviceURL) {
        int responseCode = 0;
        String httpGetResponse = "";
        try {
            URL url = new URL(serviceURL);
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setReadTimeout(120000);
            httpConn.setConnectTimeout(120000);
            httpConn.setRequestMethod("GET");
            httpConn.setDoInput(true);
            httpConn.setUseCaches(false);
            httpConn.connect();
            responseCode = httpConn.getResponseCode();
            BufferedReader _buff = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
            StringBuffer response = new StringBuffer();

            String _InputLine;
            while ((_InputLine = _buff.readLine()) != null) {
                response.append(_InputLine);
            }
            httpGetResponse = String.valueOf(responseCode);
            return httpGetResponse;
        } catch (Exception e) {
            httpGetResponse = String.valueOf(responseCode);
            e.printStackTrace();
        }
        return httpGetResponse;
    }

    public String httpPostMethod(String SERVICE_URL, HashMap<String, String> hash) {

        int responseCode = 0;
        String responseMessage;
        try {
            URL e = new URL(SERVICE_URL);
            String params = "un="+ hash.get("un")+"&pw="+hash.get("pw");
            byte[] postData = params.getBytes(StandardCharsets.UTF_8);
            HttpURLConnection conn = (HttpURLConnection) e.openConnection();
            conn.setReadTimeout(120000);
            conn.setConnectTimeout(120000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.connect();
            OutputStream out = conn.getOutputStream();
            out.write(postData);
            out.close();
            responseCode = conn.getResponseCode();
            if(responseCode == 200) {
                getDataString(hash);
                BufferedReader _buff = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuffer response = new StringBuffer();

                String _InputLine;
                while ((_InputLine = _buff.readLine()) != null) {
                    response.append(_InputLine);
                }
                if(response.toString().length() > 0) {
                    return  responseMessage = String.valueOf(responseCode) + "|" + response.toString();
                } else {
                    return  responseMessage = String.valueOf(responseCode);
                }

            }
            else
            {
                getDataString(hash);
                String x = conn.getClass().toString();
                InputStreamReader ip = new InputStreamReader(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(ip);
                StringBuffer response = new StringBuffer();

                String _InputLine;
                while ((_InputLine = reader.readLine()) != null) {
                    response.append(_InputLine);
                }
                if(response.toString().length() > 0) {
                    return  responseMessage = String.valueOf(responseCode) + "|" + response.toString();
                } else {
                    return  responseMessage = String.valueOf(responseCode);
                }
            }
        } catch (Exception var11) {
            responseMessage = String.valueOf(responseCode);
        }
        return responseMessage;
    }

    private String getDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return result.toString();
    }


    public String httpPostMethodJSON(String SERVICE_URL, JSONObject json) {

        int responseCode = 0;
        String data = "";
        String responseMessage;
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = (HttpURLConnection) new URL(SERVICE_URL).openConnection();
            httpURLConnection.setRequestMethod("POST");

            httpURLConnection.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
            wr.writeBytes(json.toString());
            wr.flush();
            wr.close();

            InputStream in = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(in);

            int inputStreamData = inputStreamReader.read();
            while (inputStreamData != -1) {
                char current = (char) inputStreamData;
                inputStreamData = inputStreamReader.read();
                data += current;
            }
            return data;
        } catch (Exception var11) {
            responseMessage = String.valueOf(responseCode);
            var11.printStackTrace();
        }
        return data;
    }



    public String httpPostMethodStores(String SERVICE_URL, HashMap<String, String> hash) {

        int responseCode = 0;
        String responseMessage;
        try {
            URL e = new URL(SERVICE_URL);
            String params = "userid="+ hash.get("userid");
            byte[] postData = params.getBytes(StandardCharsets.UTF_8);
            HttpURLConnection conn = (HttpURLConnection) e.openConnection();
            conn.setReadTimeout(120000);
            conn.setConnectTimeout(120000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.connect();
            OutputStream out = conn.getOutputStream();
            out.write(postData);
            out.close();
            responseCode = conn.getResponseCode();
            if(responseCode == 200) {
                getDataString(hash);
                BufferedReader _buff = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuffer response = new StringBuffer();

                String _InputLine;
                while ((_InputLine = _buff.readLine()) != null) {
                    response.append(_InputLine);
                }
                if(response.toString().length() > 0) {
                    return  responseMessage = String.valueOf(responseCode) + "|" + response.toString();
                } else {
                    return  responseMessage = String.valueOf(responseCode);
                }

            }
            else
            {
                getDataString(hash);
                String x = conn.getClass().toString();
                InputStreamReader ip = new InputStreamReader(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(ip);
                StringBuffer response = new StringBuffer();

                String _InputLine;
                while ((_InputLine = reader.readLine()) != null) {
                    response.append(_InputLine);
                }
                if(response.toString().length() > 0) {
                    return  responseMessage = String.valueOf(responseCode) + "|" + response.toString();
                } else {
                    return  responseMessage = String.valueOf(responseCode);
                }
            }
        } catch (Exception var11) {
            responseMessage = String.valueOf(responseCode);
        }
        return responseMessage;
    }

}
