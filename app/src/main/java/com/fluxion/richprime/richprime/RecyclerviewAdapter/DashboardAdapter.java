package com.fluxion.richprime.richprime.RecyclerviewAdapter;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fluxion.richprime.richprime.Activity.ContainerActivity;
import com.fluxion.richprime.richprime.Beans.Branch;
import com.fluxion.richprime.richprime.Beans.Product;
import com.fluxion.richprime.richprime.Fragments.Details;
import com.fluxion.richprime.richprime.Global.Variables;
import com.fluxion.richprime.richprime.R;

import java.util.ArrayList;

/**
 * Created by fluxion inc on 27/06/2017.
 */

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.DashboardAdapterViewHolder> {
    private ArrayList<Branch> lDetail;
    private Context mContext;
    private View rootView;
    public OpenActivity openNewAcitivy;

    public interface OpenActivity {
        void openAct(View v, int position, ArrayList<Branch> plist, int storeId);
    }

    public void setOpenMethod(OpenActivity listener) {
        this.openNewAcitivy = listener;
    }
    public DashboardAdapter(ArrayList<Branch> list, Context context) {
        this.lDetail = list;
        this.mContext = context;
    }
    @Override
    public DashboardAdapter.DashboardAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dashboard, parent, false);
        return new DashboardAdapterViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(DashboardAdapter.DashboardAdapterViewHolder holder, final int position) {
        holder.tvStreet.setText(lDetail.get(position).getStoreadd());
        holder.tvBranch.setText(lDetail.get(position).getStorename());
        boolean status = lDetail.get(position).isStoreflag();
        if(status) {
            holder.ivMark.setBackground(mContext.getDrawable(R.drawable.ic_checkmark_dashboard));
        } else {
            holder.ivMark.setBackground(mContext.getDrawable(R.drawable.ic_pending));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openNewAcitivy.openAct(v, position, lDetail, lDetail.get(position).getStoreid());
                Variables.storeid = lDetail.get(position).getStoreid();
            }
        });
    }

    @Override
    public int getItemCount() {
        return lDetail.size();
    }

    public class DashboardAdapterViewHolder extends RecyclerView.ViewHolder {
        TextView tvBranch, tvStreet;
        ImageView ivMark;
        public DashboardAdapterViewHolder(View itemView) {
            super(itemView);
            tvBranch = (TextView) itemView.findViewById(R.id.tvItmDBBranchName);
            tvStreet = (TextView) itemView.findViewById(R.id.tvItmDBStreetName);
            ivMark = (ImageView) itemView.findViewById(R.id.ivItmDBXmark);
        }
    }


}
