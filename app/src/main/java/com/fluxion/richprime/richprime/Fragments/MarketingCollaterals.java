package com.fluxion.richprime.richprime.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.fluxion.richprime.richprime.Activity.ContainerActivity;
import com.fluxion.richprime.richprime.Activity.SignatureActivity;
import com.fluxion.richprime.richprime.Global.Functions;
import com.fluxion.richprime.richprime.R;

import java.io.ByteArrayOutputStream;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MarketingCollaterals.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MarketingCollaterals#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MarketingCollaterals extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int CLIPSTRIP = 10;
    private static final int POSTER = 20;
    private static final int CDU = 30;
    private static final int HEADER = 40;
    private static final int OTHERTOYS_PSM = 50;
    private static final int RGI = 60;
    private static final int OTHERTOYS_PROMOTION= 70;

    private Switch swClipStrip, swPoster, swCDU, swHeader, swOthersPSM, swRGI, swOthersPromotion;
    private Button btnClipStrip, btnPoster, btnCDU, btnHeader, btnRGI;
    private ImageView ivClipStrip, ivPoster, ivCDU, ivHeader, ivRGI;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public MarketingCollaterals() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MarketingCollaterals.
     */
    // TODO: Rename and change types and number of parameters
    public static MarketingCollaterals newInstance(String param1, String param2) {
        MarketingCollaterals fragment = new MarketingCollaterals();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_marketing_collaterals, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Functions.setUpActionBar(getActivity(), "Marketing Collaterals", new StoreAudit());
        ((ContainerActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((ContainerActivity) getActivity()).getSupportActionBar().setTitle("Marketing Collaterals");
        initialize(view);
        viewActions();

        Button b = (Button) view.findViewById(R.id.btnNext);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.nextFragment(getActivity(), new SAF());

            }
        });
    }

    private void viewActions() {
        //Clipstrip
        swClipStrip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    btnClipStrip.setVisibility(View.VISIBLE);
                    btnClipStrip.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, CLIPSTRIP);
                        }
                    });
                } else {
                    btnClipStrip.setVisibility(View.GONE);
                    ivClipStrip.setVisibility(View.GONE);
                    ivClipStrip.setImageBitmap(null);
                }
            }
        });

        //Poster
        swPoster.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    btnPoster.setVisibility(View.VISIBLE);
                    btnPoster.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, POSTER);
                        }
                    });
                } else {
                    btnPoster.setVisibility(View.GONE);
                    ivCDU.setVisibility(View.GONE);
                    ivCDU.setImageBitmap(null);
                }
            }
        });

        //CDU
        swCDU.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    btnCDU.setVisibility(View.VISIBLE);
                    btnCDU.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, CDU);
                        }
                    });
                } else {
                    btnCDU.setVisibility(View.GONE);
                    ivCDU.setVisibility(View.GONE);
                    ivCDU.setImageBitmap(null);
                }
            }
        });

        //Header
        swHeader.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    btnHeader.setVisibility(View.VISIBLE);
                    btnHeader.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, HEADER);
                        }
                    });
                } else {
                    btnHeader.setVisibility(View.GONE);
                    ivHeader.setVisibility(View.GONE);
                    ivHeader.setImageBitmap(null);

                }
            }
        });

        //Other toys PSM
        swOthersPSM.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {

                } else {
                }
            }
        });

        //RGI
        swRGI.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    btnRGI.setVisibility(View.VISIBLE);
                    btnRGI.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, RGI);
                        }
                    });
                } else {
                    btnRGI.setVisibility(View.GONE);
                    ivRGI.setVisibility(View.GONE);
                    ivRGI.setImageBitmap(null);
                }
            }
        });

        //Other Toys PRomotion
        swOthersPromotion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {

                } else {
                }
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void initialize(View v) {
        //Switches
        swClipStrip = (Switch) v.findViewById(R.id.sw_mc_clipstrip);
        swPoster = (Switch) v.findViewById(R.id.sw_mc_poster);
        swCDU = (Switch) v.findViewById(R.id.sw_mc_cdu);
        swHeader = (Switch) v.findViewById(R.id.sw_mc_header);
        swOthersPSM = (Switch) v.findViewById(R.id.sw_mc_othertoys);
        swRGI = (Switch) v.findViewById(R.id.sw_mc_rgi);
        swOthersPromotion = (Switch) v.findViewById(R.id.sw_mc_otherpromotion);

        //Buttons
        btnClipStrip = (Button) v.findViewById(R.id.btn_mc_addphoto_clipstrip);
        btnPoster = (Button) v.findViewById(R.id.btn_mc_addphoto_poster);
        btnCDU = (Button) v.findViewById(R.id.btn_mc_addphoto_cdu);
        btnHeader = (Button) v.findViewById(R.id.btn_mc_addphoto_header);
        btnRGI = (Button) v.findViewById(R.id.btn_mc_addphoto_rgi);

        //ImageViews
        ivClipStrip = (ImageView) v.findViewById(R.id.iv_mc_clipstrip);
        ivPoster = (ImageView) v.findViewById(R.id.iv_mc_poster);
        ivCDU = (ImageView) v.findViewById(R.id.iv_mc_cdu);
        ivHeader = (ImageView) v.findViewById(R.id.iv_mc_header);
        ivRGI = (ImageView) v.findViewById(R.id.iv_mc_rgi);

    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            photo = Bitmap.createScaledBitmap(photo, 100, 100, false);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
            switch (requestCode) {
                case 10:
                    clipstrip(photo);
                    break;
                case 20:
                    poster(photo);
                    break;
                case 30 :
                    cdu(photo);
                    break;
                case 40:
                    header(photo);
                    break;
                case 60:
                    rgi(photo);
                    break;

            }
        } else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(getActivity(), "Cancelled", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Failed to capture an image", Toast.LENGTH_SHORT).show();
        }

    }

    public void clipstrip(Bitmap photo) {
        ivClipStrip.setImageBitmap(photo);
        ivClipStrip.setVisibility(View.VISIBLE);
        btnClipStrip.setVisibility(View.GONE);
    }

    public void poster(Bitmap photo) {
        ivPoster.setImageBitmap(photo);
        ivPoster.setVisibility(View.VISIBLE);
        btnPoster.setVisibility(View.GONE);
    }

    public void cdu(Bitmap photo) {
        ivCDU.setImageBitmap(photo);
        ivCDU.setVisibility(View.VISIBLE);
        btnCDU.setVisibility(View.GONE);
    }

    public void header(Bitmap photo) {
        ivHeader.setImageBitmap(photo);
        ivHeader.setVisibility(View.VISIBLE);
        btnHeader.setVisibility(View.GONE);
    }


    public void rgi(Bitmap photo) {
        ivRGI.setImageBitmap(photo);
        ivRGI.setVisibility(View.VISIBLE);
        btnRGI.setVisibility(View.GONE);
    }



}
