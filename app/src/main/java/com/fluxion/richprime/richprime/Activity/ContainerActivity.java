package com.fluxion.richprime.richprime.Activity;

import android.net.Uri;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.fluxion.richprime.richprime.Beans.Branch;
import com.fluxion.richprime.richprime.Fragments.Details;
import com.fluxion.richprime.richprime.Fragments.MarketingCollaterals;
import com.fluxion.richprime.richprime.Fragments.SAF;
import com.fluxion.richprime.richprime.Fragments.StoreAudit;
import com.fluxion.richprime.richprime.R;

import java.util.ArrayList;

public class ContainerActivity extends AppCompatActivity implements Details.OnFragmentInteractionListener, MarketingCollaterals.OnFragmentInteractionListener, StoreAudit.OnFragmentInteractionListener,
        SAF.OnFragmentInteractionListener

{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        int idx = getIntent().getExtras().getInt("Index");
        int storeid = getIntent().getExtras().getInt("StoreId");
        ArrayList<Branch> list = (ArrayList<Branch>) getIntent().getExtras().getSerializable("Details");
        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Bundle bundle = new Bundle();
            Fragment frag = new Details();
            Log.v("CONTAINER ACT", "");
            bundle.putInt("Index", idx);
            bundle.putInt("StoreId", storeid);
            bundle.putSerializable("Details", list);
            frag.setArguments(bundle);
            ft.replace(R.id.fragment_container, frag);
            ft.commit();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
              //  Toast.makeText(ContainerActivity.this, "Test Back button", Toast.LENGTH_SHORT).show();
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);


    }
}
