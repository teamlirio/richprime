package com.fluxion.richprime.richprime.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.fluxion.richprime.richprime.Activity.ContainerActivity;
import com.fluxion.richprime.richprime.Activity.SignatureActivity;
import com.fluxion.richprime.richprime.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SAF.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SAF#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SAF extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView rvItems;
    private Button btnAddSig;
    private OnFragmentInteractionListener mListener;

    public SAF() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SAF.
     */
    // TODO: Rename and change types and number of parameters
    public static SAF newInstance(String param1, String param2) {
        SAF fragment = new SAF();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sa, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       // Functions.setUpActionBar(getActivity(), "Suggestions and Feedback", new MarketingCollaterals());
        ((ContainerActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((ContainerActivity) getActivity()).getSupportActionBar().setTitle("Suggestions and Feedback");
        rvItems = (RecyclerView) view.findViewById(R.id.rvsaf);
        btnAddSig = (Button) view.findViewById(R.id.btn_saf_addSig);

        btnAddSig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i  = new Intent(getActivity(), SignatureActivity.class);
                startActivity(i);
            }
        });
        rvItems.setLayoutManager(new LinearLayoutManager(getActivity()));
        SAFAdapter saf = new SAFAdapter(getActivity(), filler());
        rvItems.setAdapter(saf);



    }

    public ArrayList<String> filler() {
        ArrayList<String> fill = new ArrayList<>();
        fill.add("Hot Wheels Basic Car (Qty by 72's)");
        fill.add("Barbie Minis (Qty by 12's)");
        fill.add("Shopkins 2's (Qty by 30's)");
        fill.add("Thomas Minis (Qty by 24's)");
        fill.add("Uno Cards (Qty by 24's)");
        fill.add("Disney Placemats (Qty by 30's)");
        fill.add("Scrabble (Qty by 30's)");
        return fill;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class SAFAdapter extends RecyclerView.Adapter<SAFAdapter.SAFViewHolder> {
        private Context mContext;
        private ArrayList<String> list;
        private View rootView;
        public SAFAdapter(Context context, ArrayList<String> list ) {
            this.list = list;
            this.mContext = context;
        }
        @Override
        public SAFAdapter.SAFViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_saf, parent, false);
            return new SAFAdapter.SAFViewHolder(rootView);
        }

        @Override
        public void onBindViewHolder(SAFAdapter.SAFViewHolder holder, int position) {
            holder.tvName.setText(list.get(position).toString());
            holder.etItem.setText("0");
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class SAFViewHolder extends RecyclerView.ViewHolder {
            TextView tvName;
            EditText etItem;
            public SAFViewHolder(View itemView) {
                super(itemView);
                tvName = (TextView) itemView.findViewById(R.id.tvItem_saf_header);
                etItem = (EditText) itemView.findViewById(R.id.etItem_saf_input);
            }
        }
    }
}
