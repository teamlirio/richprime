package com.fluxion.richprime.richprime.Fragments;

import android.app.assist.AssistStructure;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.fluxion.richprime.richprime.Activity.ContainerActivity;
import com.fluxion.richprime.richprime.Beans.Product;
import com.fluxion.richprime.richprime.Database.DBHandler;
import com.fluxion.richprime.richprime.Global.Functions;
import com.fluxion.richprime.richprime.Global.Variables;
import com.fluxion.richprime.richprime.R;
import com.fluxion.richprime.richprime.RecyclerviewAdapter.StoreAuditAdapter;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StoreAudit.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StoreAudit#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StoreAudit extends Fragment implements StoreAuditAdapter.OpenCamera {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView rvProductList;
    private Button btnAddPhoto;
    private DBHandler dbHandler;
    private ImageView ivSA_parent1, ivSA_parent2;
    private boolean isFull, isOne, isTwo;
    private ArrayList<Product> plist;
    private StoreAuditAdapter saa;
    private int currentPosition;


    private OnFragmentInteractionListener mListener;

    public StoreAudit() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment StoreAudit.
     */
    // TODO: Rename and change types and number of parameters
    public static StoreAudit newInstance(String param1, String param2) {
        StoreAudit fragment = new StoreAudit();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_store_audit, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        dbHandler = new DBHandler(getActivity());
        // Functions.setUpActionBar(getActivity(), "Store Audit", new Details());
        ((ContainerActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((ContainerActivity) getActivity()).getSupportActionBar().setTitle("Store Audit");
        btnAddPhoto = (Button) view.findViewById(R.id.btn_storeaudit_addphoto);
        rvProductList = (RecyclerView) view.findViewById(R.id.rvITem_StoreAudit);
        ivSA_parent1 = (ImageView) view.findViewById(R.id.iv_storeaudit_1);
        ivSA_parent2 = (ImageView) view.findViewById(R.id.iv_storeaudit_2);
        plist = filler();
        saa = new StoreAuditAdapter(getActivity(), plist);
        rvProductList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvProductList.setAdapter(saa);
        saa.setOpenCameraMethod(this);

        View v = rvProductList.getRootView();
        if (v != null) {

            Toast.makeText(getActivity(), "NOT NULL" + v.getId(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "NULL", Toast.LENGTH_SHORT).show();
        }
        rvProductList.setItemViewCacheSize(plist.size());
        btnAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                isFull = false;
                Variables.fromAdapter = false;
            }
        });
        Button b = (Button) view.findViewById(R.id.btnNext);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.nextFragment(getActivity(), new MarketingCollaterals());
            }
        });


    }

    public void captureImage() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v("REQ CODE:", "Request Code : " + requestCode);
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            Log.v("TEST", "result code: " + resultCode);
            Log.v("TEST", "Result : " + RESULT_OK);
            if (resultCode == RESULT_OK) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                photo = Bitmap.createScaledBitmap(photo, 100, 100, false);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
                if (!Variables.fromAdapter) {
                    if (!isFull) {
                        if (ivSA_parent1.getDrawable() == null) {
                            ivSA_parent1.setImageBitmap(photo);
                            ivSA_parent1.setVisibility(View.VISIBLE);
                            isOne = true;
                            Variables.fromAdapter = false;
                        } else if (ivSA_parent2.getDrawable() == null) {
                            ivSA_parent2.setVisibility(View.VISIBLE);
                            ivSA_parent2.setImageBitmap(photo);
                            isTwo = true;
                            Variables.fromAdapter = false;
                        }
                    } else {
                        if (isOne) {
                            ivSA_parent1.setImageBitmap(photo);
                        }

                        if (isTwo) {
                            ivSA_parent2.setImageBitmap(photo);
                        }
                    }

                    if (ivSA_parent1.getDrawable() != null) {
                        ivSA_parent1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                                isFull = true;
                                isOne = true;
                                isTwo = false;
                                Variables.fromAdapter = false;
                            }
                        });
                    }

                    if (ivSA_parent2.getDrawable() != null) {
                        ivSA_parent2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                                isFull = true;
                                isTwo = true;
                                isOne = false;
                                Variables.fromAdapter = false;
                            }
                        });
                    }
                } else {
                    //Data from adapter
                    Log.v("IS FROM ADAPTER", "YES");
                    onCaptureImageResult(data);

                }


            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getActivity(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getActivity(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }

    }


    public ArrayList<Product> filler() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product("Hot Wheels", false, false, false, false, false, View.GONE, View.VISIBLE));
        list.add(new Product("Shopkins 2s", false, false, false, false, false, View.GONE, View.VISIBLE));
        list.add(new Product("Uno Cards", false, false, false, false, false, View.GONE, View.VISIBLE));
        list.add(new Product("Disney Placemats", false, false, false, false, false, View.GONE, View.VISIBLE));
        list.add(new Product("Scrabble", false, false, false, false, false, View.GONE, View.VISIBLE));
        list.add(new Product("Finding Dory", false, false, false, false, false, View.GONE, View.VISIBLE));
        return list;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void addPhoto(View v, int position) {
        currentPosition = position;
        captureImage();
        Variables.fromAdapter = true;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void onCaptureImageResult(Intent data) {

        Bundle extras = data.getExtras();
        Bitmap imageBitmap = (Bitmap) extras.get("data");
        imageBitmap = Bitmap.createScaledBitmap(imageBitmap, 100, 100, false);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
        // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
        Uri tempUri = getImageUri(getActivity(), imageBitmap, "tmp");
        String picturePath = getRealPathFromURI(tempUri);
        saa.setImageInItem(currentPosition, imageBitmap, picturePath);

    }

    public Uri getImageUri(Context inContext, Bitmap inImage, String imageName) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, imageName, null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
