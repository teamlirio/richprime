package com.fluxion.richprime.richprime.Global;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fluxion.richprime.richprime.Activity.ContainerActivity;
import com.fluxion.richprime.richprime.Activity.MainActivity;
import com.fluxion.richprime.richprime.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

/**
 * Created by fluxion inc on 11/07/2017.
 */

public class Functions {


    public static void nextFragment(Activity activity, Fragment nextFrag) {
       FragmentTransaction ft = ((AppCompatActivity)activity).getSupportFragmentManager().beginTransaction();
       ft.replace(R.id.fragment_container, nextFrag).addToBackStack(null);
       ft.commit();
   }


    public static void showErrorMessage(Activity activity, String message, String title) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setMessage(message);
        alert.setTitle(title);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog a = alert.create();
        a.show();
    }


    public static List<JSONObject> convertToList(JSONArray jsonArray) {
        List<JSONObject> list = new ArrayList<JSONObject>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                list.add(jsonArray.getJSONObject(i));
            } catch (JSONException e) {
                System.out.println("Unable to convert to list");
                e.printStackTrace();
            }
        }
        return list;
    }




}
