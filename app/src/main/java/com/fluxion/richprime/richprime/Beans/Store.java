package com.fluxion.richprime.richprime.Beans;

import android.graphics.Bitmap;

/**
 * Created by fluxion inc on 22/08/2017.
 */

public class Store {
    private int storeid;
    private boolean newdeliveries;
    private String checkindt;
    private Bitmap checkinimg;
    private double checkinlat;
    private double checkinlong;
    private String pricefeedback;
    private String comment;
    private boolean othertoys;
    private boolean rgipromo;
    private String storefeedback;
    private String feedbackjobtitle;
    private String feedbackperson;
    private Bitmap signature;

    public int getStoreid() {
        return storeid;
    }

    public void setStoreid(int storeid) {
        this.storeid = storeid;
    }

    public boolean isNewdeliveries() {
        return newdeliveries;
    }

    public void setNewdeliveries(boolean newdeliveries) {
        this.newdeliveries = newdeliveries;
    }

    public String getCheckindt() {
        return checkindt;
    }

    public void setCheckindt(String checkindt) {
        this.checkindt = checkindt;
    }

    public Bitmap getCheckinimg() {
        return checkinimg;
    }

    public void setCheckinimg(Bitmap checkinimg) {
        this.checkinimg = checkinimg;
    }

    public double getCheckinlat() {
        return checkinlat;
    }

    public void setCheckinlat(double checkinlat) {
        this.checkinlat = checkinlat;
    }

    public double getCheckinlong() {
        return checkinlong;
    }

    public void setCheckinlong(double checkinlong) {
        this.checkinlong = checkinlong;
    }

    public String getPricefeedback() {
        return pricefeedback;
    }

    public void setPricefeedback(String pricefeedback) {
        this.pricefeedback = pricefeedback;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isOthertoys() {
        return othertoys;
    }

    public void setOthertoys(boolean othertoys) {
        this.othertoys = othertoys;
    }

    public boolean isRgipromo() {
        return rgipromo;
    }

    public void setRgipromo(boolean rgipromo) {
        this.rgipromo = rgipromo;
    }

    public String getStorefeedback() {
        return storefeedback;
    }

    public void setStorefeedback(String storefeedback) {
        this.storefeedback = storefeedback;
    }

    public String getFeedbackjobtitle() {
        return feedbackjobtitle;
    }

    public void setFeedbackjobtitle(String feedbackjobtitle) {
        this.feedbackjobtitle = feedbackjobtitle;
    }

    public String getFeedbackperson() {
        return feedbackperson;
    }

    public void setFeedbackperson(String feedbackperson) {
        this.feedbackperson = feedbackperson;
    }

    public Bitmap getSignature() {
        return signature;
    }

    public void setSignature(Bitmap signature) {
        this.signature = signature;
    }
}
