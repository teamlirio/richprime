package com.fluxion.richprime.richprime.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fluxion.richprime.richprime.API.RichPrimeAPI;
import com.fluxion.richprime.richprime.Beans.Branch;
import com.fluxion.richprime.richprime.Global.Functions;
import com.fluxion.richprime.richprime.Global.Variables;
import com.fluxion.richprime.richprime.R;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.Permission;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LoginActivity extends AppCompatActivity {


    private EditText etUser, etPass;
    private Button btnLogin;
    private  boolean _permissions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        checkPermissions();
        etUser = (EditText) findViewById(R.id.etLoginUserName);
        etPass = (EditText) findViewById(R.id.etLoginPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        etUser.setText("norms");
        etPass.setText("123");
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LoginUser login = new LoginUser();
                String un = etUser.getText().toString();
                String pw = etPass.getText().toString();
                if(_permissions) {
                    if (un != null && pw != null) {

                        login.execute(un, pw);
                    }
                } else {
                    checkPermissions();
                }

            }
        });
    }

    public void checkPermissions() {

        ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.CAMERA}, 1);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "GRANTED!", Toast.LENGTH_SHORT).show();
                    _permissions = true;
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    _permissions = false;
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(LoginActivity.this, "This app requires the said permissions", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }


    public class LoginUser extends AsyncTask<String, String, String> {
        ProgressDialog pd = new ProgressDialog(LoginActivity.this);
        Branch branch;
        String _res = "";
        ArrayList<Branch> list = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Checking credentials..");
            pd.setCancelable(true);
            pd.setTitle("Login");
            pd.show();

        }

        @Override
        protected String doInBackground(String... params) {
            HashMap<String, String> hash = new HashMap<>();
            hash.put("un", params[0]);
            hash.put("pw", params[1]);
            String res = new RichPrimeAPI(LoginActivity.this).getUserCredentials(hash);
            String pipe = "|";
            if (res.contains(pipe)) {
                String[] split = res.split("\\|");
                String jsonData = split[1];
                Log.v("LOGIN", jsonData);
                try {
                    List<JSONObject> rootList = Functions.convertToList(new JSONArray(jsonData));
                    for (JSONObject jRoot : rootList) {
                        if (jRoot.getString("response").equalsIgnoreCase("success")) {
                            _res = "success";
                            List<JSONObject> dataList = Functions.convertToList(jRoot.getJSONArray("data"));
                            for (JSONObject data : dataList) {
                                List<JSONObject> useracctList = Functions.convertToList(data.getJSONArray("useracct"));
                                for (JSONObject useracct : useracctList) {
                                    System.out.println("acctid: " + useracct.getString("acctid"));
                                    System.out.println("ufname: " + useracct.getString("ufname"));
                                    Variables.userid = useracct.getInt("acctid");
                                }
                            }
                        } else {
                            _res = jRoot.getString("description");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                if (res.equalsIgnoreCase("500")) {
                    _res = "Server is down";
                } else {
                    _res = "Login failed. Please try again later.";
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (_res.equalsIgnoreCase("success")) {
                String json = new Gson().toJson(list);
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.putExtra("BDetail", json);
                startActivity(intent);
                finish();
            } else if (_res.equalsIgnoreCase("Server is down")) {
                Functions.showErrorMessage(LoginActivity.this, _res, "Login Error");
            } else if (_res.equalsIgnoreCase("invalid")) {
                Functions.showErrorMessage(LoginActivity.this, _res, "Login Error");
            }
        }
    }
}
