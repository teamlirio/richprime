package com.fluxion.richprime.richprime.RecyclerviewAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.fluxion.richprime.richprime.Activity.ContainerActivity;
import com.fluxion.richprime.richprime.Beans.Product;
import com.fluxion.richprime.richprime.Fragments.StoreAudit;
import com.fluxion.richprime.richprime.Global.Variables;
import com.fluxion.richprime.richprime.R;

import java.util.ArrayList;

/**
 * Created by fluxion inc on 19/07/2017.
 */

public class StoreAuditAdapter extends RecyclerView.Adapter<StoreAuditAdapter.StoreAuditViewHolder> {

    private Context mContext;
    private View rootView;
    private ArrayList<Product> list;
    private OpenCamera openCamera;
    public interface OpenCamera {
        void addPhoto(View v, int position);
    }

    public void setOpenCameraMethod(OpenCamera listener) {
        this.openCamera = listener;
    }

    public StoreAuditAdapter(Context context, ArrayList<Product> list) {
        this.mContext = context;
        this.list = list;
    }

    public void setRootView(View rootView) {
        Log.v("ROOTVIEW", "SET");
        this.rootView = rootView;
    }

    public View getRootView() {
        Log.v("ROOTVIEW", "GET");
        return rootView;
    }

    @Override
    public StoreAuditAdapter.StoreAuditViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_store_audit, parent, false);
        setRootView(rootView);
        return new StoreAuditViewHolder(rootView);

    }

    @Override
    public void onBindViewHolder(final StoreAuditAdapter.StoreAuditViewHolder holder, final int position) {
        final Product p = list.get(position);
        holder.tvName.setText(list.get(position).getName());
        p.setPosition(position);
        holder.btnAddPhoto.setTag(position);
        holder.ivItem_1.setTag(position);
        holder.ivItem_2.setTag(position);
        holder.etQty.setTag(position);
        holder.etPrice.setTag(position);
        holder.etOfftake.setTag(position);

        if (list.get(Variables.currentPosition).is1HasImg()) {
            holder.ivItem_1.setImageBitmap(p.getBitmap1());
            holder.ivItem_1.setVisibility(View.VISIBLE);
            list.get(Variables.currentPosition).setOne(true);
            if(list.get(Variables.currentPosition).isMainSwitchOn()) {
                Log.v("MAIN SWITCH", "IS ON!");
                holder.swToggle.setOnCheckedChangeListener(p.getMainsSwitch());
                holder.swToggle.setChecked(p.isMainSwitchOn());
                holder.llContainer.setVisibility(p.getIsContainerVisible());

            }
            if(list.get(Variables.currentPosition).isPlanoOn()) {
                holder.swPlano.setChecked(p.isPlanoOn());
                holder.btnAddPhoto.setVisibility(p.getAddPhotoVisibility());
                holder.btnAddPhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (openCamera != null) {
                            openCamera.addPhoto(v, (int) holder.btnAddPhoto.getTag());
                        }

                    }
                });
            }

            Log.v("NORMZ", "POS in first imageview is " + Variables.currentPosition );
        }
        if (list.get(Variables.currentPosition).isOne()) {
            Log.v("NORMZ", "POS in 2nd imageview is " + Variables.currentPosition );
            holder.ivItem_2.setImageBitmap(p.getBitmap2());
            holder.ivItem_2.setVisibility(View.VISIBLE);

        }

        holder.etQty.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    Toast.makeText(mContext, "has", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "no ", Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.swPlano.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    holder.btnAddPhoto.setEnabled(true);
                    holder.ivItem_1.setClickable(true);
                    holder.ivItem_2.setClickable(true);
                    holder.btnAddPhoto.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (openCamera != null) {
                                Log.v("NORMZ", "Position passed : " + position);
                                Log.v("NORMZ", "Product Position passed : " + p.getPosition());
                                Log.v("NORMZ", "Tag Position passed : " + holder.btnAddPhoto.getTag());
                                Variables.currentPosition = (int) holder.btnAddPhoto.getTag();
                                openCamera.addPhoto(v, (int) holder.btnAddPhoto.getTag());
                            }

                        }
                    });
                } else {
                    holder.btnAddPhoto.setEnabled(false);
                    holder.ivItem_1.setClickable(false);
                    holder.ivItem_2.setClickable(false);
                }
            }
        });
        holder.swToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    p.setPlanoOn(true);
                    holder.llContainer.setVisibility(View.VISIBLE);

                } else {
                    holder.etQty.setText("");
                    holder.etOfftake.setText("");
                    holder.etPrice.setText("");
                    holder.swToggle.setChecked(false);
                    holder.ivItem_1.setImageBitmap(null);
                    holder.ivItem_2.setImageBitmap(null);
                    p.setMainSwitchOn(false);
                    holder.llContainer.setVisibility(View.GONE);
                }
            }
        });


    }


    public void setImageInItem(int position, Bitmap imgSrc, String imgPath) {
        Product product = list.get(Variables.currentPosition);
        if (product.isOne()) {
            Log.v("POS 2", "" + product.getPosition());
            product.setBitmap2(imgSrc);
            product.setIs2HasImg(true);
            product.setOne(true);
            product.setPlanoOn(true);
            product.setMainSwitchOn(true);
            product.setIsContainerVisible(View.VISIBLE);
            product.setMainsSwitch(null);
            product.setAddPhotoVisibility(View.GONE);
        } else {
            Log.v("POS 1", "" + product.getPosition());
            product.setBitmap1(imgSrc);
            product.setIs1HasImg(true);
            product.setOne(true);
            product.setPlanoOn(true);
            product.setAddPhotoVisibility(View.VISIBLE);
            product.setMainSwitchOn(true);
            product.setIsContainerVisible(View.VISIBLE);
            product.setMainsSwitch(null);

        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class StoreAuditViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;
        private EditText etQty, etPrice, etOfftake;
        private Switch swToggle, swPlano;
        private LinearLayout llContainer;
        private Button btnAddPhoto;
        private TableRow tbContainer;
        private ImageView ivItem_1, ivItem_2;


        public StoreAuditViewHolder(View itemView) {
            super(itemView);

            tvName = (TextView) rootView.findViewById(R.id.tv_storeaudit_header);
            etQty = (EditText) rootView.findViewById(R.id.et_storeaudit_quantity);
            etPrice = (EditText) rootView.findViewById(R.id.et_storeaudit_pricing);
            etOfftake = (EditText) rootView.findViewById(R.id.et_storeaudit_storeofftake);
            swToggle = (Switch) rootView.findViewById(R.id.sw_storeaudit);
            llContainer = (LinearLayout) rootView.findViewById(R.id.llitem_storeaudit_container);
            swPlano = (Switch) rootView.findViewById(R.id.sw_storeaudit_plano);
            btnAddPhoto = (Button) rootView.findViewById(R.id.btnItem_storeaudit_addphoto);
            tbContainer = (TableRow) rootView.findViewById(R.id.tbitem_storeaudit_container);
            ivItem_1 = (ImageView) rootView.findViewById(R.id.ivitem_storeaudit_1);
            ivItem_2 = (ImageView) rootView.findViewById(R.id.ivitem_storeaudit_2);
        }


    }


}
