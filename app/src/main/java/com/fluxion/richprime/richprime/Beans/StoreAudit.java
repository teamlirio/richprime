package com.fluxion.richprime.richprime.Beans;

/**
 * Created by fluxion inc on 25/07/2017.
 */

public class StoreAudit {

    private int id;
    private byte[] data;
    private double lat;
    private double lng;
    private int storeId;
    private String position;

    public StoreAudit() {
    }

    public StoreAudit(int id, byte[] data, double lat, double lng, int storeId, String position) {
        this.id = id;
        this.data = data;
        this.lat = lat;
        this.lng = lng;
        this.storeId = storeId;
        this.position = position;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
