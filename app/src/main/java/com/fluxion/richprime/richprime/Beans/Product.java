package com.fluxion.richprime.richprime.Beans;

import android.graphics.Bitmap;
import android.widget.CompoundButton;

/**
 * Created by fluxion inc on 19/07/2017.
 */

public class Product {

    private String name;
    private int quantity;
    private double pricing;
    private int offtake;
    private String planograms;
    private Bitmap bmPhoto;
    private int position;
    private Bitmap bitmap1;
    private Bitmap bitmap2;
    private boolean is1HasImg;
    private boolean is2HasImg;
    private boolean isOne;
    private boolean isPlanoOn;
    private boolean isMainSwitchOn;
    private int isContainerVisible;
    private CompoundButton.OnCheckedChangeListener mainsSwitch;
    private int addPhotoVisibility;

    public Product(String name, boolean is1HasImg, boolean is2HasImg, boolean isOne, boolean isPlanoOn, boolean isMainSwitchOn, int isContainerVisible, int addPhotoVisibility) {
        this.name = name;
        this.is1HasImg = is1HasImg;
        this.is2HasImg = is2HasImg;
        this.isOne = isOne;
        this.isPlanoOn = isPlanoOn;
        this.isMainSwitchOn = isMainSwitchOn;
        this.isContainerVisible = isContainerVisible;
        this.addPhotoVisibility = addPhotoVisibility;
    }

    public int getAddPhotoVisibility() {
        return addPhotoVisibility;
    }

    public void setAddPhotoVisibility(int addPhotoVisibility) {
        this.addPhotoVisibility = addPhotoVisibility;
    }

    public int getIsContainerVisible() {
        return isContainerVisible;
    }

    public void setIsContainerVisible(int isContainerVisible) {
        this.isContainerVisible = isContainerVisible;
    }

    public CompoundButton.OnCheckedChangeListener getMainsSwitch() {
        return mainsSwitch;
    }

    public void setMainsSwitch(CompoundButton.OnCheckedChangeListener mainsSwitch) {
        this.mainsSwitch = mainsSwitch;
    }

    public boolean isMainSwitchOn() {
        return isMainSwitchOn;
    }

    public void setMainSwitchOn(boolean mainSwitchOn) {
        isMainSwitchOn = mainSwitchOn;
    }

    public boolean isPlanoOn() {
        return isPlanoOn;
    }

    public void setPlanoOn(boolean planoOn) {
        isPlanoOn = planoOn;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Product() {
    }

    public boolean isOne() {
        return isOne;
    }

    public void setOne(boolean one) {
        isOne = one;
    }

    public Bitmap getBitmap1() {
        return bitmap1;
    }

    public void setBitmap1(Bitmap bitmap1) {
        this.bitmap1 = bitmap1;
    }

    public Bitmap getBitmap2() {
        return bitmap2;
    }

    public void setBitmap2(Bitmap bitmap2) {
        this.bitmap2 = bitmap2;
    }

    public boolean is1HasImg() {
        return is1HasImg;
    }

    public void setIs1HasImg(boolean is1HasImg) {
        this.is1HasImg = is1HasImg;
    }

    public boolean is2HasImg() {
        return is2HasImg;
    }

    public void setIs2HasImg(boolean is2HasImg) {
        this.is2HasImg = is2HasImg;
    }

    public Bitmap getBmPhoto() {
        return bmPhoto;
    }

    public void setBmPhoto(Bitmap bmPhoto) {
        this.bmPhoto = bmPhoto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPricing() {
        return pricing;
    }

    public void setPricing(double pricing) {
        this.pricing = pricing;
    }

    public int getOfftake() {
        return offtake;
    }

    public void setOfftake(int offtake) {
        this.offtake = offtake;
    }

    public String getPlanograms() {
        return planograms;
    }

    public void setPlanograms(String planograms) {
        this.planograms = planograms;
    }
}
