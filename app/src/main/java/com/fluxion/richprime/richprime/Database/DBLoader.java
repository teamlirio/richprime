package com.fluxion.richprime.richprime.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.fluxion.richprime.richprime.Beans.Store;
import com.fluxion.richprime.richprime.Beans.StoreAudit;

import java.io.ByteArrayOutputStream;

/**
 * Created by fluxion inc on 25/07/2017.
 */

public class DBLoader extends DBHandler {

    private Context mContext;
    private SQLiteDatabase db;
    private Cursor res;
    private String sql = "";

    public DBLoader(Context context) {
        super(context);
    }


    public String test() {
        return "test success";
    }

    public boolean saveCheckInDetails(Store store){
        db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("newdeliveries", store.isNewdeliveries());
        cv.put("checkindt", store.getCheckindt());
        cv.put("checkinimg", getBytes(store.getCheckinimg()));
        cv.put("checkinlat", store.getCheckinlat());
        cv.put("checkinlong", store.getCheckinlong());
        cv.put("pricefeedback", store.getPricefeedback());
        cv.put("comment", store.getComment());
        cv.put("storeid", store.getStoreid());
        cv.put("othertoys", store.isOthertoys());
        cv.put("rgipromo", store.isRgipromo());
        cv.put("storefeedback", store.getPricefeedback());
        cv.put("feedbackjobtitle", store.getFeedbackjobtitle());
        cv.put("feedbackperson", store.getFeedbackperson());
        cv.put("signature", getBytes(store.getSignature()));
        boolean insertSuccess = db.insert("StoreAudit", null, cv ) > 0;
        db.close();
        return insertSuccess;

    }

    public boolean getCheckInDetails(int storeid) {
        Log.v("RES", "STORE ID : " + storeid);
        boolean store = false;
        String sql = "Select * from StoreAudit where storeid = " + storeid;
        db = this.getWritableDatabase();
        Cursor res = db.rawQuery(sql, null);
        while(res.moveToNext()) {

            Log.v("RES", String.valueOf(res.getInt(res.getColumnIndex("storeid"))));
        }
       if(res.getCount() > 0) {

           store = true;
       }
       return store;
    }

    // convert from bitmap to byte array
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if(bitmap != null ){
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        }
        return stream.toByteArray();

    }

    // convert from byte array to bitmap
    public static Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

}
