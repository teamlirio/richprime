package com.fluxion.richprime.richprime.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fluxion.richprime.richprime.API.RichPrimeAPI;
import com.fluxion.richprime.richprime.Activity.ContainerActivity;
import com.fluxion.richprime.richprime.Activity.EditDetails;
import com.fluxion.richprime.richprime.Activity.LoginActivity;
import com.fluxion.richprime.richprime.Beans.Branch;
import com.fluxion.richprime.richprime.Beans.Store;
import com.fluxion.richprime.richprime.Database.DBLoader;
import com.fluxion.richprime.richprime.Global.Functions;
import com.fluxion.richprime.richprime.Activity.MainActivity;
import com.fluxion.richprime.richprime.Global.Variables;
import com.fluxion.richprime.richprime.R;
import com.fluxion.richprime.richprime.Util.GPSTracker;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Details.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Details#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Details extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private TextView tvStoreName, tvStoreCode, tvAddress, tvCluster, tvCPerson, tvCNumber, tvBdate;
    private Button btnCheckDetails, btnCheckIn;
    private LinearLayout llLocVerify;
    private String branchString;
    private int branchIndex, storeid;
    private GPSTracker gps;
    private int index;
    private OnFragmentInteractionListener mListener;
    private DBLoader db;
    private Bitmap bm;
    double lat, lng;
    private Date currentTime;
    ProgressDialog pd;

    public Details() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Details.
     */
    // TODO: Rename and change types and number of parameters
    public static Details newInstance(String param1, String param2) {
        Details fragment = new Details();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Functions.setUpActionBar(getActivity(), "Details");
        ((ContainerActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((ContainerActivity) getActivity()).getSupportActionBar().setTitle("Details");
        tvAddress = (TextView) view.findViewById(R.id.tvBranchDetails_Address);
        tvStoreName = (TextView) view.findViewById(R.id.tvBranchDetails_StoreName);
        tvStoreCode = (TextView) view.findViewById(R.id.tvBranchDetails_StoreCode);
        tvCluster = (TextView) view.findViewById(R.id.tvBranchDetails_ClusterType);
        tvCPerson = (TextView) view.findViewById(R.id.tvBranchDetails_ContactPerson);
        tvCNumber = (TextView) view.findViewById(R.id.tvBranchDetails_ContactNumber);
        tvBdate = (TextView) view.findViewById(R.id.tvBranchDetails_bdate);
        btnCheckDetails = (Button) view.findViewById(R.id.btnBranchDetails_CheckDetails);
        btnCheckIn = (Button) view.findViewById(R.id.btnBranchDetails_CheckIn);
        llLocVerify = (LinearLayout) view.findViewById(R.id.llBranchDetails_LocVerify);
        btnCheckDetails.setEnabled(false);
        db = new DBLoader(getActivity());
        btnCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLocationEnabled()) {
                  currentTime = Calendar.getInstance().getTime();
                    gps = new GPSTracker(getActivity());
                   lat = 0.0;
                    lng = 0.0;
                    boolean getGps = false;

                    if (gps.canGetLocation()) {
                        lat = gps.getLatitude();
                        lng = gps.getLongitude();
                        getGps = true;
                    } else {
                        // can't get location
                        // GPS or Network is not enabled
                        // Ask user to enable GPS/network in settings
                        gps.showSettingsAlert();
                        getGps = false;
                    }
                    if (getGps) {


                        Toast.makeText(getActivity(), "LAT :" + lat + "\nLNG :" + lng, Toast.LENGTH_SHORT).show();
                    } else {
                        Functions.showErrorMessage(getActivity(), "Failed to get Location. Please try again.", "Location Error");
                    }
                } else {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setMessage("Location not enabled");
                    dialog.setPositiveButton("Open location settings", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub
                            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            getActivity().startActivity(myIntent);
                            //get gps
                        }
                    });
                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            // TODO Auto-generated method stub

                        }
                    });
                    dialog.show();
                }

                captureImage();
            }
        });


        btnCheckDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.nextFragment(getActivity(), new StoreAudit());
            }
        });
        Refresh refresh = new Refresh();
        refresh.execute(Variables.userid);


        Bundle bundle = this.getArguments();
        if (bundle != null) {
            ArrayList<Branch> list = (ArrayList<Branch>) bundle.getSerializable("Details");
            index = bundle.getInt("Index");
            branchString = new Gson().toJson(list);
            branchIndex = index;
            storeid = bundle.getInt("StoreId");
            Log.v("BUNDLE", "STORE ID BUNDLE " + storeid);
            Log.v("BUNDLE", "INDEX BUNDLE " + index);
            if(db.getCheckInDetails(storeid)) {
                btnCheckIn.setEnabled(false);
                llLocVerify.setVisibility(View.VISIBLE);
                btnCheckDetails.setEnabled(true);
            }
            tvAddress.setText(list.get(index).getStoreadd());
            tvStoreName.setText(list.get(index).getStorename());
            tvStoreCode.setText(String.valueOf(list.get(index).getStorecode()));
            tvCluster.setText(list.get(index).getClustertype());
            tvCPerson.setText(list.get(index).getContactperson());
            tvCNumber.setText(list.get(index).getContactnum());
            tvBdate.setText(list.get(index).getDob());

        }

        setHasOptionsMenu(true);


    }

    public void captureImage() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }


    public boolean isLocationEnabled() {
        boolean isEnabled = false;
        LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);

        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled || !network_enabled) {
            isEnabled = false;
        } else {
            isEnabled = true;
        }

        return isEnabled;

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_edit:
                Intent intent = new Intent(getActivity(), EditDetails.class);
                Bundle bundle = new Bundle();
                bundle.putString("Details", branchString);
                bundle.putInt("Idx", branchIndex);
                intent.putExtras(bundle);
                startActivityForResult(intent, 5);
                // Toast.makeText(getActivity(), "Test Settings", Toast.LENGTH_SHORT).show();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 5) {
            Variables.refresh = true;
            Refresh refresh = new Refresh();
            refresh.execute(Variables.userid);
            getActivity().setResult(3);
        } else if(requestCode == 6) {

        } else {
            if (resultCode == RESULT_OK) {
                Log.v("ON ACT RESULT", "RESULT OK");
                if (requestCode  == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                    Log.v("ON ACT RESULT", "RESULT CODE SUCCESS " + resultCode);

                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    photo = Bitmap.createScaledBitmap(photo, 100, 100, false);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    photo.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
                    bm = photo;
                    SaveCheckInDetails scid = new SaveCheckInDetails();
                    scid.execute();

                }
            }else {
                btnCheckIn.setEnabled(true);
                llLocVerify.setVisibility(View.GONE);
                btnCheckDetails.setEnabled(false);
                Functions.showErrorMessage(getActivity(), "Please capture an image", "OOPS!");
            }


        }


    }

    @Override
    public void onPause() {
        super.onPause();
        pd.dismiss();
    }

    public class SaveCheckInDetails extends AsyncTask<String, String, String> {

        boolean isSuccess;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Saving details...");
            pd.setCancelable(true);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Store store = new Store();
            store.setStoreid(storeid);
            store.setCheckindt(String.valueOf(currentTime));
            store.setCheckinlat(lat);
            store.setCheckinlong(lng);
            store.setCheckinimg(bm);
            store.setComment("");
            store.setPricefeedback("");
            store.setNewdeliveries(false);
            store.setRgipromo(false);
            store.setOthertoys(false);
            store.setFeedbackperson("");
            store.setFeedbackjobtitle("");
            store.setSignature(null);
            store.setStorefeedback("");
            isSuccess = db.saveCheckInDetails(store);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if(isSuccess) {
                Log.v("SUCCESS", "Saved check in details!");
                btnCheckIn.setEnabled(false);
                llLocVerify.setVisibility(View.VISIBLE);
                btnCheckDetails.setEnabled(true);
            } else {
                Log.v("FAIL", "Oops! failed saving check in details");
            }
        }
    }

    public class Refresh extends AsyncTask<Integer, String, Integer> {
        Branch branch;
        String _res = "";
        ArrayList<Branch> list;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Refreshing details...");
            pd.setCancelable(true);
            pd.setTitle("Details");
            pd.show();

        }

        @Override
        protected Integer doInBackground(Integer... params) {
            try {
                HashMap<String, String> map = new HashMap<>();
                map.put("userid", String.valueOf(params[0]));
                String res = new RichPrimeAPI(getActivity()).getStores(map);

                String pipe = "|";
                if (res.contains(pipe)) {
                    String[] split = res.split("\\|");
                    String jsonData = split[1];
                    Log.v("LOGIN", jsonData);
                    List<JSONObject> rootList = Functions.convertToList(new JSONArray(jsonData));
                    for (JSONObject jRoot : rootList) {
                        if (jRoot.getString("response").equalsIgnoreCase("success")) {
                            _res = "success";
                            JSONObject jData = new JSONObject(jRoot.get("data").toString());

                            JSONArray jStores = new JSONArray(jData.get("stores").toString());
                            List<JSONObject> jStoresObject = Functions.convertToList(jStores);
                            list = new ArrayList<>();
                            for (JSONObject jsonObject : jStoresObject) {
                                Gson gson = new Gson();
                                branch = gson.fromJson(jsonObject.toString(), Branch.class);

                                list.add(branch);
                            }
                            Log.v("JDATA", jStores.toString());
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Integer s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (_res.equalsIgnoreCase("success") && branch != null) {
                if (storeid == list.get(index).getStoreid()) {
                    Log.v("Store ID", storeid + " is the store id.");
                }
                tvAddress.setText(list.get(index).getStoreadd());
                tvStoreName.setText(list.get(index).getStorename());
                tvStoreCode.setText(String.valueOf(list.get(index).getStorecode()));
                tvCluster.setText(list.get(index).getClustertype());
                tvCPerson.setText(list.get(index).getContactperson());
                tvCNumber.setText(list.get(index).getContactnum());
                tvBdate.setText(list.get(index).getDob());
            } else {
                Toast.makeText(getActivity(), "Fail refresh", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
