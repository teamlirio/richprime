package com.fluxion.richprime.richprime.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.fluxion.richprime.richprime.Global.Variables;

/**
 * Created by fluxion inc on 24/08/2017.
 */

public class DBStoreAudit extends DBHandler {
    private Context mContext;
    private SQLiteDatabase db;
    private Cursor res;
    private String sql = "";

    public DBStoreAudit(Context context) {
        super(context);
    }

    public void insertToProductAudit(String field, int tag, int value) {
        String sql = "UPDATE ProductAudit SET " + field + " = " + value + ", storeid = "+ Variables.storeid + " where tag = " + tag + ";" +
                "INSERT INTO ProductAudit (" + field + ", tag, storeid) SELECT " + value + ", " + tag + ", " + Variables.storeid + " WHERE " +
                "(Select Changes() = 0);" ;

        db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        while(cursor.moveToNext()) {
            Log.v("DB STORE AUDIT", cursor.getString(cursor.getColumnIndex("qty")));
        }
    }
}
