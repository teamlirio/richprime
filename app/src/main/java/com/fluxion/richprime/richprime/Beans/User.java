package com.fluxion.richprime.richprime.Beans;

/**
 * Created by fluxion inc on 15/08/2017.
 */

public class User {
    private int accntid;
    private String ufname;
    private String umname;
    private String ulname;
    private int role;
    private String area;

    public int getAccntid() {
        return accntid;
    }

    public void setAccntid(int accntid) {
        this.accntid = accntid;
    }

    public String getUfname() {
        return ufname;
    }

    public void setUfname(String ufname) {
        this.ufname = ufname;
    }

    public String getUmname() {
        return umname;
    }

    public void setUmname(String umname) {
        this.umname = umname;
    }

    public String getUlname() {
        return ulname;
    }

    public void setUlname(String ulname) {
        this.ulname = ulname;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
