package com.fluxion.richprime.richprime.Activity;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.fluxion.richprime.richprime.API.RichPrimeAPI;
import com.fluxion.richprime.richprime.Beans.Branch;
import com.fluxion.richprime.richprime.Beans.Product;
import com.fluxion.richprime.richprime.Beans.User;
import com.fluxion.richprime.richprime.Fragments.Details;
import com.fluxion.richprime.richprime.Global.Functions;
import com.fluxion.richprime.richprime.Global.Variables;
import com.fluxion.richprime.richprime.R;
import com.fluxion.richprime.richprime.RecyclerviewAdapter.DashboardAdapter;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private FragmentManager fm;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private RecyclerView rvItmDashboard;
    private Branch bd;
    private DashboardAdapter adapter;
    private TextView tvArea;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List of Location");
        tvArea = (TextView) findViewById(R.id.tvDashboard_Area);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        rvItmDashboard = (RecyclerView) findViewById(R.id.rvItmDashboard);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Refresh refresh = new Refresh();
        refresh.execute(Variables.userid);
        Log.v("MAIN", "ON CREATE");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Refresh refresh = new Refresh();
        refresh.execute(Variables.userid);
        Log.v("MAIN", "ON RESUME");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.v("MAIN", "ON RESTART");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
            alert.setTitle("Sign out");
            alert.setMessage(getResources().getString(R.string.signout));
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            AlertDialog a = alert.create();
            a.show();
            // super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.sign_out) {
            // Handle the camera action
            AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
            alert.setTitle("Sign out");
            alert.setMessage("Are you sure you want to sign out?");
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            AlertDialog a = alert.create();
            a.show();

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 3) {
            Refresh refresh = new Refresh();
            refresh.execute(Variables.userid);

        }
    }

    public class Refresh extends AsyncTask<Integer, String, Integer> implements DashboardAdapter.OpenActivity {
        ProgressDialog pd = new ProgressDialog(MainActivity.this);
        Branch branch;
        String _res = "";
        String area = "";
        private ArrayList<Branch> _storelistlist;
        private ArrayList<User> userList;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Getting details...");
            pd.setCancelable(true);
            pd.setTitle("Details");
            pd.show();

        }

        @Override
        protected Integer doInBackground(Integer... params) {

            try {
                HashMap<String, String> map = new HashMap<>();
                map.put("userid", String.valueOf(params[0]));
                String res = new RichPrimeAPI(MainActivity.this).getStores(map);

                String pipe = "|";
                if (res.contains(pipe)) {
                    String[] split = res.split("\\|");
                    String jsonData = split[1];
                    Log.v("LOGIN", jsonData);
                    List<JSONObject> rootList = Functions.convertToList(new JSONArray(jsonData));
                    for (JSONObject jRoot : rootList) {
                        if (jRoot.getString("response").equalsIgnoreCase("success")) {
                            _res = "success";
                            JSONObject jData = new JSONObject(jRoot.get("data").toString());
                            area = jData.getString("area");

                            JSONArray jStores = new JSONArray(jData.get("stores").toString());
                            List<JSONObject> jStoresObject = Functions.convertToList(jStores);
                            _storelistlist = new ArrayList<>();
                            for(JSONObject jsonObject : jStoresObject) {
                                Gson gson = new Gson();
                                branch = gson.fromJson(jsonObject.toString(), Branch.class);
                                _storelistlist.add(branch);
                            }
                            Log.v("JDATA", jStores.toString());
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Integer s) {
            super.onPostExecute(s);
            pd.dismiss();
            if (_res.equalsIgnoreCase("success")) {
                Log.v("ON POST EXEC", _res);
                tvArea.setText(area);
                rvItmDashboard.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                adapter = new DashboardAdapter(_storelistlist, MainActivity.this);
                rvItmDashboard.setAdapter(adapter);
                adapter.setOpenMethod(this);
                rvItmDashboard.setClickable(true);

            } else {
                // Toast.makeText(getActivity(), "Fail refresh", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void openAct(View v, int position, ArrayList<Branch> plist, int storeId) {
            Intent intent = new Intent(MainActivity.this, ContainerActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("Index", position);
            bundle.putSerializable("Details", plist);

            bundle.putInt("StoreId", storeId);
            Log.v("MAIN ACT", "STORE ID " + storeId);
            Log.v("MAIN ACT", "POSITION " + position);
            intent.putExtras(bundle);
            startActivityForResult(intent, 3);
        }
    }

}
