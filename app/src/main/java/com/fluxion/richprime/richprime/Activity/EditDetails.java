package com.fluxion.richprime.richprime.Activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.annotation.BoolRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fluxion.richprime.richprime.API.RichPrimeAPI;
import com.fluxion.richprime.richprime.Beans.Branch;
import com.fluxion.richprime.richprime.Global.Functions;
import com.fluxion.richprime.richprime.Global.Variables;
import com.fluxion.richprime.richprime.R;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class EditDetails extends AppCompatActivity {
    private EditText etStoreName, etStoreCode, etAddress, etCperson, etCnumber;
    private Spinner spClusterType;
    private TextView tvBdate;
    private Branch branch;
    private Button btnCancel, btnSave;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_details);
        etStoreName = (EditText) findViewById(R.id.et_editdetails_storename);
        etStoreCode = (EditText) findViewById(R.id.et_editdetails_storecode);
        etAddress = (EditText) findViewById(R.id.et_editdetails_address);
        etCperson = (EditText) findViewById(R.id.et_editdetails_contactperson);
        etCnumber = (EditText) findViewById(R.id.et_editdetails_contactnumber);
        spClusterType = (Spinner) findViewById(R.id.sp_editdetails_clustertype);
        tvBdate = (TextView) findViewById(R.id.tv_editdetails_bdate);
        btnCancel = (Button) findViewById(R.id.btn_editdetails_cancel);
        btnSave = (Button) findViewById(R.id.btn_editdetails_save);

        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                tvBdate.setText(sdf.format(myCalendar.getTime()));

            }

        };

        tvBdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EditDetails.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        String data = getIntent().getExtras().getString("Details");
        int idx = getIntent().getExtras().getInt("Idx");
        Log.v("DATA Edit Details", data);
        ArrayList<Branch> list = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            List<JSONObject> storeList = Functions.convertToList(jsonArray);

            for (JSONObject store : storeList) {
                Gson gson = new Gson();
                Log.v("Edit Details", "GSON Process..");
                branch = gson.fromJson(store.toString(), Branch.class);
                list.add(branch);
            }
        } catch (JSONException e) {
            Log.v("Edit Details", "Something went wrong");
            e.printStackTrace();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditDetails.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.clustertype));
        spClusterType.setAdapter(adapter);

        int index = adapter.getPosition(list.get(idx).getClustertype());

        spClusterType.setSelection(index);
        etStoreName.setText(list.get(idx).getStorename());
        etStoreCode.setText(String.valueOf(list.get(idx).getStorecode()));
        etAddress.setText(list.get(idx).getStoreadd());
        etCperson.setText(list.get(idx).getContactperson());
        etCnumber.setText(list.get(idx).getContactnum());
        tvBdate.setText(list.get(idx).getDob());
        id = list.get(idx).getStoreid();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(6);
                finish();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String storeName = etStoreName.getText().toString();
                String storeCode = etStoreCode.getText().toString();
                String address = etAddress.getText().toString();
                String cPerson = etCperson.getText().toString();
                String cNumber = etCnumber.getText().toString();
                String bdate = tvBdate.getText().toString();
                String _id = String.valueOf(id);
                String clusterType = spClusterType.getSelectedItem().toString();
               SaveDetails save = new SaveDetails();
                save.execute(_id, storeName, storeCode, address, clusterType, cPerson, cNumber, bdate);
            }
        });


    }

    private class SaveDetails extends AsyncTask<String, String, String> {
        ProgressDialog pd = new ProgressDialog(EditDetails.this);
        String result = "";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Saving Details...");
            pd.setCancelable(true);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response = "";
            try {
                JSONObject postData = new JSONObject();
                postData.put("storeid", Integer.parseInt(params[0]));
                postData.put("storename", params[1]);
                postData.put("storecode", params[2]);
                postData.put("storeadd", params[3]);
                postData.put("clustertype", params[4]);
                postData.put("contactperson", params[5]);
                postData.put("contactnum", params[6]);
                postData.put("dob", params[7]);
                String res = new RichPrimeAPI(EditDetails.this).saveEditDetails(postData);

                JSONArray jArray = new JSONArray(res);
                List<JSONObject> jList = Functions.convertToList(jArray);
                for(JSONObject jItem : jList) {
                    Log.v("J ITEM", jItem.toString());
                    JSONArray jItemArray = new JSONArray(jItem.get("data").toString());
                    List<JSONObject> _jItemArray = Functions.convertToList(jItemArray);
                    for(JSONObject _j : _jItemArray) {
                        Log.v("JARRAY data", _j.getString("message"));
                        result = _j.getString("message");
                    }

                }
                List<JSONObject> jResponse = Functions.convertToList(new JSONArray(res));
                for(JSONObject jItem : jResponse) {

                    response = jItem.getString("response");
                    Log.v("J ITEM", response);
                }
                return response;

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
            if(s.equalsIgnoreCase("success")) {

                Log.v("Success", "ON POS EXECUTE" );
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(EditDetails.this);
                alertDialog.setMessage(result);
                alertDialog.setTitle("Update Details");
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(EditDetails.this, "FIRE!!", Toast.LENGTH_SHORT).show();
                        setResult(5);
                        dialog.dismiss();
                        finish();
                    }
                });
                AlertDialog a = alertDialog.create();
                a.show();
            } else {
                Toast.makeText(EditDetails.this, "Failed!", Toast.LENGTH_SHORT).show();
            }
        }
    }


}
