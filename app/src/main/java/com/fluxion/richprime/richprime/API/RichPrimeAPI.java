package com.fluxion.richprime.richprime.API;

import android.content.Context;
import android.util.Log;


import com.fluxion.richprime.richprime.R;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by fluxion inc on 29/06/2017.
 */

public class RichPrimeAPI {
    private Context mContext;
    public RichPrimeAPI(Context context) {
        this.mContext = context;
    }
    public String getUserCredentials(HashMap<String, String> hash) {
        return new RestAPI().httpPostMethod(mContext.getResources().getString(R.string.api_url_login), hash);

    }

    public String getStores(HashMap<String, String> hash) {
        return new RestAPI().httpPostMethodStores(mContext.getResources().getString(R.string.api_url_getstores), hash);

    }

    public String saveEditDetails(JSONObject jsonObject) {
        Log.v("JSON DATA", jsonObject.toString());
        return  new RestAPI().httpPostMethodJSON(mContext.getResources().getString(R.string.api_url_editdetails), jsonObject);
    }


}
